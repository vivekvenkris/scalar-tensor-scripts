import numpy as np
import pylab as py

def betan(cons, alpha0):
	return 2.*cons*(1.+alpha0**2.)**2./alpha0**2.

cons=1.
avec=np.logspace(-6.,2.,50)
bvec=betan(cons, avec)
py.ion()
py.semilogy(bvec,avec)
raw_input('na')
exit()


def beta0pos(kd,alpha0):
	return (((1.+alpha0**2.)**3.*kd)*1./(4.*alpha0**2.))**(1./2.)-1.-alpha0**2.


def beta0neg(kd,alpha0):
	return -(((1.+alpha0**2.)**3.*kd)*1./(4.*alpha0**2.))**(1./2.)-1.-alpha0**2.

avec=np.logspace(-8,2,50)
kdpul=-0.8e-4

#kdpul=0.8e-4
kdpul=4.8e-3

bvecpos=beta0pos(kdpul,avec)
bvecneg=beta0neg(kdpul,avec)

py.ion()
#py.semilogy(bvecpos,avec)
#py.semilogy(bvecneg,avec)
py.plot(bvecpos,avec)
py.plot(bvecneg,avec)
py.xlim(-6.,6)
#ymin,ymax=1e-4,1e1
ymin,ymax=0.,0.1
py.ylim(ymin,ymax)
py.vlines(0.,ymin,ymax)
#py.show()
raw_input('na')
exit()

def gamma(alpha0):
	return 1.-2.*alpha0**2.*1./(1.+alpha0**2.)

def beta(alpha0, beta0):
	return 1.+0.5*(alpha0*beta0*alpha0)*1./(1.+alpha0)**2.

def eta(alpha0, beta0):
	return 4.*beta(alpha0,beta0)-gamma(alpha0)-3.

def kd(alpha0,beta0):
	return 2.*eta(alpha0,beta0)**2.*1./(1.-gamma(alpha0))

beta0=0.
alpha0vec=np.logspace(-6.,1.,200)
kdvec=kd(alpha0vec,beta0)


print kd(0.1,0.1)

