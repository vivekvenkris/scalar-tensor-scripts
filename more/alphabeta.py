#!/usr/bin/python
import numpy as np
import pylab as py

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30
day=24.*3600.

#Binary parameters from FreireEtAl2012.
ma=1.46
mb=0.181
ecc=3.4e-7
pb=0.35*day
pbdot=-17e-15

#Functions of the observed parameters.
def l1fun(ma,mb,ecc,pb,alpha0):
	return -np.pi*ma*mb*msun**2.*alpha0**2.*grav**(5./3.)/(light**5.*3.*(ma*msun+mb*msun)**(1./3.)*(1.+alpha0**2.)**(4./3.))*(2.*np.pi/pb)**(5./3.)*ecc**2.*(1.+ecc**2./4.)*1./(1.-ecc**2.)**(7./2.)

def l2fun(ma,mb,ecc,pb,alpha0):
	return -32.*np.pi*ma*mb*msun**2.*grav**(5./3.)*1./(light**5.*5.*(ma*msun+mb*msun)**(1./3.))*(2.*np.pi/pb)**(5./3.)*(1.+73.*ecc**2./24.+37.*ecc**4./96)*(1.+alpha0**2.)**(2./3.)/(1.-ecc**2.)**(7./2.)*(6.+alpha0**2.)

#def beta0pos(ma,mb,ecc,pb,pbdot,alpha0):
#	return 1./6.*(8.*(1.+alpha0**2.)+np.sqrt((pbdot-l2fun(ma,mb,ecc,pb,alpha0))*1./l1fun(ma,mb,ecc,pb,alpha0)))

#def beta0neg(ma,mb,ecc,pb,pbdot,alpha0):
#	return 1./6.*(8.*(1.+alpha0**2.)-np.sqrt((pbdot-l2fun(ma,mb,ecc,pb,alpha0))*1./l1fun(ma,mb,ecc,pb,alpha0)))

#There are problems with minus signs...
def beta0pos(ma,mb,ecc,pb,pbdot,alpha0):
	return 1./6.*(8.*(1.+alpha0**2.)+np.sqrt(abs((pbdot-l2fun(ma,mb,ecc,pb,alpha0))*1./l1fun(ma,mb,ecc,pb,alpha0))))

def beta0neg(ma,mb,ecc,pb,pbdot,alpha0):
	return 1./6.*(8.*(1.+alpha0**2.)-np.sqrt(abs((pbdot-l2fun(ma,mb,ecc,pb,alpha0))*1./l1fun(ma,mb,ecc,pb,alpha0))))

#Plot alpha_0 vs beta_0.
alpha0vec=np.logspace(-4.,0.,100)
beta0negvec=beta0neg(ma,mb,ecc,pb,pbdot,alpha0vec)
beta0posvec=beta0pos(ma,mb,ecc,pb,pbdot,alpha0vec)
py.ion()
py.semilogy(beta0negvec,alpha0vec)
py.semilogy(beta0posvec,alpha0vec)
py.xlabel('beta_0')
py.ylabel('alpha_0')
raw_input('enter')

#Alternative simplified formulas for alpha_0 and beta_0 as a function of the observed value of k_d.
def beta0pos(kd,alpha0):
	return (((1.+alpha0**2.)**3.*kd)*1./(4.*alpha0**2.))**(1./2.)-1.-alpha0**2.

def beta0neg(kd,alpha0):
	return -(((1.+alpha0**2.)**3.*kd)*1./(4.*alpha0**2.))**(1./2.)-1.-alpha0**2.

#Plot the new alpha_0 vs beta_0.
avec=np.logspace(-8,2,50)
kdpul=-0.8e-4 #kdpul=0.8e-4
kdpul=4.8e-3

bvecpos=beta0pos(kdpul,avec)
bvecneg=beta0neg(kdpul,avec)

py.clf()
py.semilogy(bvecpos,avec)
py.semilogy(bvecneg,avec)
py.xlim(-6.,6)
ymin,ymax=1e-4,1e1
py.ylim(ymin,ymax)
py.vlines(0.,ymin,ymax)
py.xlabel('beta_0')
py.ylabel('alpha_0')
raw_input('enter')
