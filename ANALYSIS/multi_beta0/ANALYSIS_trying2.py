#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
import warnings

#INPUT PARAMETERS:
polyord=5 #Order of the polynomial fit.
mbAbins=700 #Number of points in the vector of baryonic mass.
inputdir='../../../data/ANALYSIS/multi_beta0/run4/'
outputdir='../../../data/ANALYSIS/multi_beta0/run4/combined/'
print 'The output of this script has passed physical conditions (on maximum value of alpha0, certain range of radius and NS masses). But it will need further selections:'
print '(1) Take only physical solutions, i.e. those where mass (mA) decreases for increasing pressure (at the centre of the NS).'
print '(2) The theoretical alphaA and the derived one should coincide within 5 percent.'

###########################
#Create output folder if not existing.
if not os.path.isdir(outputdir): os.makedirs(outputdir)

#Define physical conditions.
absalpha0_max=1. #Maximum value of |alpha0|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=20000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.

##############
#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]

py.ion()################

param_files=np.sort([ fili for fili in os.listdir(inputdir) if fili[0:10]=='parameters'])

np.random.shuffle(param_files)########

for fili in xrange(len(param_files)):
	print 'File %i / %i .' %(fili+1, len(param_files))
	indi=param_files[fili][-7:-4]
	params=np.loadtxt(inputdir+'parameters_%s.txt' %indi)
	sols=np.loadtxt(inputdir+'solutions_%s.txt' %indi)
	ofile=outputdir+'combined_%s.txt' %indi
	
	sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
	sM=params[1] #Typical radius of the NS in m.
	psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
	rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
	mtb=params[4] #Baryonic mass (kg).
	nt0=params[5] #Baryon number density (m^-3).
	Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
	kns=params[7] #Dimensionless constant of the polytrope EoS.
	beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.

	#Variable parameters.
	#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
	pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (bar) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8]

	############################
	#Take only physical solutions.
	check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alpha0)<absalpha0_max)
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]
	
	if False:
		py.clf()
		sel=abs(phi0-0.0024)<0.0001
		py.plot(mbA[sel], -alphaA[sel], '.')
		raw_input('enter')

	#Define some vectors and matrices.
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	delta_mbA=mbAvec[1]-mbAvec[0]
	
	#Output taken (almost) directly from the results of the integrated equations.
	pcen_mat=[]
	phicen_mat=[]
	Rns_mat=[]
	alphaA_mat=[]
	phi0_mat=[]
	mA_mat=[]
	mbA_mat=[]
	IA_mat=[]
	alpha0_mat=[]

	#Output that has been derived.
	mA_calc_mat=[]
	alphaA_calc_mat=[]
	betaA_calc_mat=[]
	IA_calc_mat=[]
	kA_calc_mat=[]
	
	#Vector of baryonic mass.
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)

	for mbi in xrange(len(mbAvec)):
		selecti=(abs(mbA-mbAvec[mbi])<=delta_mbA) #Select by baryonic mass.
		#if len(selecti[selecti])<minpoints:
		#	print 'Not enough points! What should I do?'
		#	continue
		sorti=phi0[selecti].argsort() #Sort by phi0.
		phi0_s=phi0[selecti][sorti]
		mA_s=mA[selecti][sorti]
		IA_s=IA[selecti][sorti]

		pcen_mat.extend(pcen[selecti][sorti])
		phicen_mat.extend(phicen[selecti][sorti])
		Rns_mat.extend(Rns[selecti][sorti])
		alphaA_mat.extend(alphaA[selecti][sorti])
		phi0_mat.extend(phi0_s)
		mA_mat.extend(mA_s)
		mbA_mat.extend(mbA[selecti][sorti])
		IA_mat.extend(IA_s)
		alpha0_mat.extend(alpha0[selecti][sorti])

		warnings.filterwarnings('ignore') #Ignore the polyfit warning.

		lmA_fit=np.polyfit(phi0_s, np.log(mA_s), polyord)
		dlmA_fit=(np.arange(len(lmA_fit))[::-1]*lmA_fit)[:-1]
		d2lmA_fit=(np.arange(len(dlmA_fit))[::-1]*dlmA_fit)[:-1]
		lmA_pol=np.poly1d(lmA_fit)
		dlmA_pol=np.poly1d(dlmA_fit)
		d2lmA_pol=np.poly1d(d2lmA_fit)

		lIA_fit=np.polyfit(phi0_s, np.log(IA_s), polyord)
		dlIA_fit=(np.arange(len(lIA_fit))[::-1]*lIA_fit)[:-1]
		lIA_pol=np.poly1d(lIA_fit)
		dlIA_pol=np.poly1d(dlIA_fit)

		mA_calc=np.exp(lmA_pol(phi0_s))
		alphaA_calc=dlmA_pol(phi0_s)
		betaA_calc=d2lmA_pol(phi0_s)
		IA_calc=np.exp(lIA_pol(phi0_s))
		kA_calc=-dlIA_pol(phi0_s)

		alphaA_calc2=np.diff(np.log(mA_calc))*1./np.diff(phi0_s)


		if True:
			print 'beta0=%.3f, mbA=%.3f' %(beta0, mbAvec[mbi])
			py.figure(1)
			py.clf()
			py.semilogx(abs(phi0_s), abs(np.log(mA_s)), '.', color='blue', alpha=0.5)
			py.semilogx(abs(phi0_s), abs(np.log(mA_calc)), 's', color='red', alpha=0.5)
			py.figure(2)
			py.clf()
			py.loglog(abs(phi0_s*beta0), abs(alphaA[selecti][sorti]),'.', color='blue', alpha=0.5)
			py.loglog(abs(phi0_s*beta0), abs(alphaA_calc),'^', color='red', alpha=0.5)
			py.loglog(abs(phi0_s[:-1]*beta0), abs(alphaA_calc2),'s', color='green', alpha=0.5)
			##py.plot(phi0_s[:-1], np.diff(np.log(mA_calc_median_interp))*1./np.diff(phi0_s),'.',color='green')
			#py.plot(phi0_s_mean, alphaA_calc2,'s',color='green')
			raw_input('enter')

		mA_calc_mat.extend(mA_calc)
		alphaA_calc_mat.extend(alphaA_calc)
		betaA_calc_mat.extend(betaA_calc)
		IA_calc_mat.extend(IA_calc)
		kA_calc_mat.extend(kA_calc)

	if True:
		py.clf()
		phi0lim=0.0024 #Limit on phi0 from Solar System experiments (from DEF96).
		sel=(abs(np.array(phi0_mat)-phi0lim)<0.00001)
		py.plot(np.array(mbA_mat)[sel], -np.array(alphaA_mat)[sel],'.', color='blue')
		py.plot(np.array(mbA_mat)[sel], -np.array(alphaA_calc_mat)[sel],'.', color='red')
		raw_input('enter')

	o_data=np.vstack((pcen_mat, phicen_mat, Rns_mat, alphaA_mat, phi0_mat, mA_mat, mbA_mat, IA_mat, alpha0_mat, mA_calc_mat, alphaA_calc_mat, betaA_calc_mat, IA_calc_mat, kA_calc_mat)).T

	continue##########
	#Save data.
	np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n polyord=%i \n mbAbins=%i \n sp=%e \n sM=%e \n psur=%e \n rhocen=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n p_cen/Pa, phi_cen, NS_radius/m, alphaA, phi0, mA/msun, mbA/msun, IA/(kg m2), alpha0, mA_calc/msun, alphaA_calc, betaA_calc, IA_calc/(kg m2), kA_calc.' %(beta0, polyord, mbAbins, sp, sM, psur, rhocen, mtb, nt0, Gamma, kns))

