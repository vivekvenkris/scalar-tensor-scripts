#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script

(*General constants (all quantities are in SI units);*)

Gn = 6.673 10^-11; (*Newtons gravitational constant;*)

c = 2.99792458 10^8;(*Speed of light in vacuum;*)

Msun = 1.9891 10^30;(*Solar mass;*)
rsun = 6.955 10^8;(*Solar radius;*)


(*Specific input parameters;*)

sp = 10^36;(*Typical maximum pressure at the centre of the NS in Pa;*)

sM = 10^4;(*Typical radius of the NS in m;*)

pNsurT = 10^-6;(*Pressure at the surface of the NS (in theory should \
be zero, but that leads to singularities);*)

pNminT = 0.002;(*Minimum pressure at the centre of the NS in \
rescaled units (so the real value is that multiplied by sp);*)

pNmaxT = 0.2;(*Maximum pressure at the centre of the NS in rescaled \
units (so the real value is that multiplied by sp);*)
\[CurlyPhi]NminT = 0.001;
(*Minimum value of the scalar field at the centre of the NS;*)
\[CurlyPhi]NmaxT = 0.5;
(*Maximum value of the scalar field at the centre of the NS;*)
\[Rho]NcenT = 10^-2;(*Radial coordinate at the centre (in theory should be zero);*)
pbins = 400;(*Number of bins in values of pressure (log spaced);*)
\[CurlyPhi]bins = 400; (*Number of bins in values of scalar field (log spaced);*)
outputdir = "../../../data/ANALYSIS/multi_beta0/run4b/";(*Output directory of data;*)

(*Model for the Equation of state of the neutron star;*)

(*Polytrope (from DamourEspositoFarese1996);*)

mtb = 1.66 10^-27; (*Baryonic mass (kg);*)
nt0 = 10^44;(*Baryon number density (m^-3);*)
\[CapitalGamma] = 2.34; (*Dimensionless constant;*)
kns = 0.0195;(*Dimensionless constant;*)

(*Model for the scalar tensor theory*)

A[\[Beta]0_, \[CurlyPhi]_] = Exp[1/2 \[Beta]0 \[CurlyPhi]^2];(*Dimensionless function: coupling \
function.*)
\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \
\[CurlyPhi];(*Dimensionless function: linear coupling.*)