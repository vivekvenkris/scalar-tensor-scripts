#!/Applications/Mathematica.app/Contents/MacOS/MathematicaScript -script
##############
#This script should be loaded simply as "./ANALYSIS.m". The input parameters should be manually modified in "./INPUT_PARAMETERS.m".
#It solves the EspositoFarese1998 equations and outputs the data that will be analysed by "./ANALYSIS_2.py".
##############

\[Beta]0=ToExpression[$ScriptCommandLine[[2]]] (*Get beta0 from command line;*)

(*Some of the input values will lead to errors, but I don't care \
about them, since I will restrict the solutions to physical ones.*)
\
Off[NDSolve::ndsz]
On[General::stop]
Off[General::infy]
Off[General::indet]
Off[NDSolve::ndnum]
Off[InterpolatingFunction::dmval]
Off[Greater::nord]
Off[GreaterEqual::nord]
Off[Less::nord]
Off[FrontEndObject::notavail]
Off[SetDirectory::fstr]

(***********************************)

(*INPUT PARAMETERS*)

SetDirectory[
  NotebookDirectory[]];(*Set working directory to the one of this \
file;*)
NotebookEvaluate[Directory[] <> "/INPUT_PARAMETERS.m"];
<< "./INPUT_PARAMETERS.m";(*This will work when run as a script;*)


(***********************************)

(*I rescale all variables that are taking big/small numbers to avoid \
overflows*)

s\[Epsilon] = (nt0 mtb c^2)^(1 - 1/\[CapitalGamma]) sp^(
   1/\[CapitalGamma]);
sMb = 4 Pi mtb nt0 sM^3;
kM = (4 Pi Gn)/c^4 sM^2 s\[Epsilon];
k\[Nu] = (8 Pi Gn)/c^4 sM^2 sp;
kp = s\[Epsilon]/sp;
kn = s\[Epsilon]/(nt0 mtb c^2);

(*Functions and parameters that will enter the system of field \
equations and boundary conditions to be numerically solved*)

\[Epsilon]N[pN_] = (pN/kns)^(1/\[CapitalGamma]) + 
   kp^-1 pN/(\[CapitalGamma] - 1);
nN[pN_] = kn (pN/kns)^(1/\[CapitalGamma]);

MNcen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 0;
\[Nu]Ncen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 0;
\[Psi]Ncen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 
  kM (1/3 \[Rho]Ncen) A[\[Beta]0, \[CurlyPhi]Ncen]^4 \
\[Alpha][\[Beta]0, \[CurlyPhi]Ncen] (\[Epsilon]N[pNcen] - 
     3 kp^-1 pNcen);(*This function provokes problems when solving \
the equation*)
\
(*\[Psi]Ncen[\[Rho]Ncen_,pNcen_,\[CurlyPhi]Ncen_]=10^-4;*)

MbNcen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 0;
\[Omega]Ncen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 1;
\[Omega]bNcen[\[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] = 
  kM 4/5 \[Rho]Ncen A[\[Beta]0, \[CurlyPhi]Ncen]^4 (\[Epsilon]N[
      pNcen] + kp^-1 pNcen) \[Omega]Ncen[\[Rho]Ncen, 
    pNcen, \[CurlyPhi]Ncen];
(*\[Omega]bcen[\[Rho]cen_,pcen_,\[CurlyPhi]cen_]=0;*)

(*System of first order differential equations that has to be solved \
for the given boundary conditions pcen, \[CurlyPhi]cen*)

eqns = {MN'[
     pN] == (kM \[Rho]N[
         pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 \[Epsilon]N[pN] + 
       1/2 \[Rho]N[pN] (\[Rho]N[pN] - 2 MN[pN]) \[Psi]N[
         pN]^2) (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[Nu]N'[
     pN] == (k\[Nu] (\[Rho]N[
          pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
         2 MN[pN]) + \[Rho]N[pN] \[Psi]N[pN]^2 + (
       2 MN[pN])/(\[Rho]N[
         pN] (\[Rho]N[pN] - 2 MN[pN]))) (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[CurlyPhi]N'[
     pN] == \[Psi]N[
      pN] (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[Psi]N'[
     pN] == (kM (\[Rho]N[
          pN] A[\[Beta]0, \[CurlyPhi]N[pN]]^4)/(\[Rho]N[pN] - 
         2 MN[pN]) (\[Alpha][\[Beta]0, \[CurlyPhi]N[
             pN]] (\[Epsilon]N[pN] - 3 kp^-1 pN) + \[Rho]N[
            pN] \[Psi]N[pN] (\[Epsilon]N[pN] - pN)) - (
       2 (\[Rho]N[pN] - MN[pN]) \[Psi]N[pN])/(\[Rho]N[
         pN] (\[Rho]N[pN] - 2 MN[pN]))) (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[Rho]N'[
     pN] == (-kp (\[Epsilon]N[pN] + 
        kp^-1 pN) (kM/
         kp (\[Rho]N[
           pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
          2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
        MN[pN]/(\[Rho]N[
          pN] (\[Rho]N[pN] - 
           2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
          pN]))^-1,
   MbN'[pN] == (nN[pN] A[\[Beta]0, \[CurlyPhi]N[pN]]^3 \[Rho]N[pN]^2/
       Sqrt[1 - 
        2 MN[pN]/\[Rho]N[pN]]) (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[Omega]N'[
     pN] == \[Omega]bN[
      pN] (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1,
   \[Omega]bN'[
     pN] == (kM \[Rho]N[pN]^2/(\[Rho]N[pN] - 2 MN[pN])
         A[\[Beta]0, \[CurlyPhi]N[pN]]^4 (\[Epsilon]N[pN] + 
          kp^-1 pN) (\[Omega]bN[pN] + (
          4 \[Omega]N[pN])/\[Rho]N[
           pN]) + (\[Psi]N[pN]^2 \[Rho]N[pN] - 
          4/\[Rho]N[pN]) \[Omega]bN[pN]) (-kp (\[Epsilon]N[pN] + 
         kp^-1 pN) (kM/
          kp (\[Rho]N[
            pN]^2 A[\[Beta]0, \[CurlyPhi]N[pN]]^4 pN)/(\[Rho]N[pN] - 
           2 MN[pN]) + 1/2 \[Rho]N[pN] \[Psi]N[pN]^2 + 
         MN[pN]/(\[Rho]N[
           pN] (\[Rho]N[pN] - 
            2 MN[pN])) + \[Alpha][\[Beta]0, \[CurlyPhi]N[pN]] \[Psi]N[
           pN]))^-1};
bcns[\[Rho]Ncen_, 
   pNcen_, \[CurlyPhi]Ncen_] = {MN[pNcen] == 
    MNcen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Nu]N[pNcen] == \[Nu]Ncen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[CurlyPhi]N[pNcen] == \[CurlyPhi]Ncen,
   \[Psi]N[pNcen] == \[Psi]Ncen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Rho]N[pNcen] == \[Rho]Ncen,
   MbN[pNcen] == MbNcen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Omega]N[pNcen] == \[Omega]Ncen[\[Rho]Ncen, 
     pNcen, \[CurlyPhi]Ncen],
   \[Omega]bN[pNcen] == \[Omega]bNcen[\[Rho]Ncen, 
     pNcen, \[CurlyPhi]Ncen]};
bcns[\[Rho]Ncen_, 
   pNcen_, \[CurlyPhi]Ncen_] = {MN[pNcen] == 
    MNcen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Nu]N[pNcen] == \[Nu]Ncen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[CurlyPhi]N[pNcen] == \[CurlyPhi]Ncen,
   \[Psi]N[pNcen] == \[Psi]Ncen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Rho]N[pNcen] == \[Rho]Ncen,
   MbN[pNcen] == MbNcen[\[Rho]Ncen, pNcen, \[CurlyPhi]Ncen],
   \[Omega]N[pNcen] == \[Omega]Ncen[\[Rho]Ncen, 
     pNcen, \[CurlyPhi]Ncen],
   \[Omega]bN[pNcen] == \[Omega]bNcen[\[Rho]Ncen, 
     pNcen, \[CurlyPhi]Ncen]};
sol[pNsur_, \[Rho]Ncen_, pNcen_, \[CurlyPhi]Ncen_] := 
  NDSolve[{eqns, 
    bcns[\[Rho]Ncen, 
     pNcen, \[CurlyPhi]Ncen]}, {MN, \[Nu]N, \[CurlyPhi]N, \[Psi]N, \
\[Rho]N, MbN, \[Omega]N, \[Omega]bN}, {pN, pNsur, pNcen}];

(*Define the functions linspace and logspace;*)

linspace[start_, stop_, n_: 100] := 
 Table[x, {x, start, stop, (stop - start)/(n - 1)}]
linspace[start_, stop_, 1] := Mean[{start, stop}]
logspace[a_, b_, n_] := 10.0^Range[a, b, (b - a)/(n - 1)]

(*pNcenVEC=linspace[pNminT,pNmaxT,pbins];*)

pNcenVEC = logspace[Log10[pNminT], Log10[pNmaxT], pbins];
(*\[CurlyPhi]NcenVEC=linspace[\[CurlyPhi]NminT,\[CurlyPhi]NmaxT,\
\[CurlyPhi]bins];*)
\[CurlyPhi]NcenVEC = 
  logspace[Log10[\[CurlyPhi]NminT], 
   Log10[\[CurlyPhi]NmaxT], \[CurlyPhi]bins];

Clear[sols]; sols = {}; Do[
 pNcenT = pNcenVEC[[pi]];
 \[CurlyPhi]NcenT = \[CurlyPhi]NcenVEC[[\[CurlyPhi]i]];
 soli = sol[pNsurT, \[Rho]NcenT, pNcenT, \[CurlyPhi]NcenT];
 (*If NDSolve managed to solve it, the length of soli should be 1:*)
 
 If[Length[soli] != 1, Continue[]];
 MNsol[pN_] = Evaluate[MN[pN] /. First[soli]];
 (*Check that the solution has a domain that is not just a point*)
 \
(*domain=IntegerPart[MNsol[pN][[0]][[1]][[1]]];
 If[IntegerPart[domain[[2]]-domain[[1]]]<1,Continue[]];*)
 \[Nu]Nsol[
   pN_] = Evaluate[\[Nu]N[pN] /. First[soli]];
 \[CurlyPhi]Nsol[pN_] = Evaluate[\[CurlyPhi]N[pN] /. First[soli]];
 \[Psi]Nsol[pN_] = Evaluate[\[Psi]N[pN] /. First[soli]];
 \[Rho]Nsol[pN_] = Evaluate[\[Rho]N[pN] /. First[soli]];
 MbNsol[pN_] = Evaluate[MbN[pN] /. First[soli]];
 \[Omega]Nsol[pN_] = Evaluate[\[Omega]N[pN] /. First[soli]];
 \[Omega]bNsol[pN_] = Evaluate[\[Omega]bN[pN] /. First[soli]];
 Rns = \[Rho]Nsol[pNsurT] sM;
 \[Nu]ps = 
  Rns (\[Psi]Nsol[pNsurT]/sM)^2 + (2 MNsol[pNsurT] sM)/(
   Rns (Rns - 2 MNsol[pNsurT] sM));
 \[Alpha]A = (2 \[Psi]Nsol[pNsurT]/sM)/\[Nu]ps;
 Q1 = Sqrt[1 + \[Alpha]A^2];
 Q2 = Sqrt[1 - (2 MNsol[pNsurT] sM)/Rns];
 \[Nu]cs = -(2/Q1) ArcTanh[Q1/(1 + 2/(Rns \[Nu]ps))];
 \[CurlyPhi]0 = \[CurlyPhi]Nsol[pNsurT] - 1/2 \[Alpha]A \[Nu]cs;
 mA = c^2/Gn 1/2 \[Nu]ps Rns^2 Q2 Exp[1/2 \[Nu]cs];
 mbA = MbNsol[pNsurT] sMb;
 JA = c^2/Gn 1/
   6 (\[Omega]bNsol[pNsurT]/sM) Rns^4 Q2 Exp[-(1/2) \[Nu]cs];
 \[CapitalOmega] = \[Omega]Nsol[pNsurT] - 
   c^4/Gn (3 JA)/(
    4 mA^3 (3 - \[Alpha]A^2)) (Exp[2 \[Nu]cs] - 
      1 + (4 Gn mA)/(Rns c^2)
        Exp[\[Nu]cs] ((2 Gn mA)/(Rns c^2) + 
         Exp[1/2 \[Nu]cs] Cosh[1/2 Q1 \[Nu]cs]));
 IA = JA/\[CapitalOmega];
 \[Alpha]0 = \[Beta]0 \[CurlyPhi]0;
 (*Now perform a few safety checks:*)
 (*Check that the values of \
\[Alpha]0 and \[Beta]0 are within the values allowed by Solar System \
experiments.*)
 (*Check that \[CurlyPhi]cen \[CurlyPhi]0 >0. As \
explained in DamourEspositoFarese1998 (referring to \
DamourEspositoFarese1996), configurations where that product is \
negative are energetically disfavored.*)
 
 cond1 = (\[CurlyPhi]NcenT \[CurlyPhi]0 >= 0) ;
 cond2 = (\[Alpha]0 < 1);
 If[cond1 && cond2,
  AppendTo[
   sols, { pNcenT 1., \[CurlyPhi]NcenT 1., 
    Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, 
    IA 1., \[Alpha]0 1.}]], {pi, 1, Length[pNcenVEC]}, {\[CurlyPhi]i, 
  1, Length[\[CurlyPhi]NcenVEC]}]

params = {}; AppendTo[params, {sp 1., sM 1., 
  pNsurT 1., \[Rho]NcenT 1., mtb 1., nt0 1., \[CapitalGamma] 1., 
  kns 1., \[Beta]0 1.}];

(*Check name of last created file and create name of new parameter \
and solutions files;*)

files = FileNames["parameters_*.txt", {outputdir}];
paramfiles = {}; Do[
 AppendTo[paramfiles, 
  ToExpression[StringTake[files[[fili]], {-7, -5}]]], {fili, 1, 
  Length[files]}];
If[Length[paramfiles] == 0, numnewfile = 1, 
  numnewfile = Max[paramfiles] + 1];
outputparamfile = 
  "parameters_" <> IntegerString[numnewfile, 10, 3] <> ".txt";
outputsolfile = 
  "solutions_" <> IntegerString[numnewfile, 10, 3] <> ".txt";

(*Export data;*)

Export[outputdir <> outputparamfile, params, "Table"];
Export[outputdir <> outputsolfile, sols, "Table"];