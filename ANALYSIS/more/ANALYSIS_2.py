#!/usr/bin/python
##############
#This script should:
#(1) Load the data produced by mathematica and apply some physical checks on it.
#(2) Export data that can be used by Vivek's mass-mass script.
##############

import numpy as np
import pylab as py

outputplotdir='../../plots/initial_guess/' #Output directory for plots.

#Define physical conditions.
absalphaA_max=1. #Maximum value of |alphaA|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=15000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30

#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]
params=np.loadtxt('../../data/ANALYSIS/initial_guess/parameters.txt') #All fixed parameters.
sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
sM=params[1] #Typical radius of the NS in m.
psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
mtb=params[4] #Baryonic mass (kg).
nt0=params[5] #Baryon number density (m^-3).
Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
kns=params[7] #Dimensionless constant of the polytrope EoS.
beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.
#Variable parameters.
#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
sols=np.loadtxt('../../data/ANALYSIS/initial_guess/solutions.txt') #All variable parameters.
pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
Rns=sols[:,2] #Radius of the NS in m.
alphaA=sols[:,3] #Effective coupling (dimensionless).
phi0=sols[:,4] #Scalar field at infinity (dimensionless).
mA=sols[:,5] #Mass in solar masses.
mbA=sols[:,6] #Mass (bar) in solar masses.
IA=sols[:,7] #Moment of inertia in kg m^2.
alpha0=sols[:,8]

#Take only physical solutions.
check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alphaA)<absalphaA_max)
#Select only those solutions that are consistent with Solar System experiments.
#phi0lim=0.0043 #Limit on phi0 from Solar System experiments (old value from DEF93).
phi0lim=0.0024 #Limit on phi0 from Solar System experiments (old value from DEF96).
#sel=(abs(phi0-phi0lim)<phi0lim*0.05) #This selects only one stripe at the maximum value.
sel=check&(phi0<phi0lim)
#sel=check&(phi0<phi0lim)&(1.3<mA)&(mA<1.4)

print 'By looking at the following plot I can choose a better parameter space of p and phi at the centre. There are many points in uninteresting areas!'
print 'I could output points in the accepted region, and use them as inputs for ANALYSIS, to calculate the equations in those areas.'
py.ion()
py.loglog(pcen,phicen,'.',color='green',label='All')
py.loglog(pcen[check],phicen[check],'.',color='blue',label='Physical')
py.loglog(pcen[sel],phicen[sel],'.',color='red',label='Unconstrained')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('Scalar field at the centre of the NS')
py.legend(loc='upper left')
py.savefig(outputplotdir+'phicen_vs_pcen.png')
raw_input('enter')

#Apply physical conditions to all quantities.
#pcen=pcen[check]
#phicen=phicen[check]
#Rns=Rns[check]
#alphaA=alphaA[check]
#phi0=phi0[check]
#mA=mA[check]
#mbA=mbA[check]
#IA=IA[check]
#alpha0=alpha0[check]

#Some plots.
py.clf()
py.plot(mA[sel],-alphaA[sel],'.')
py.plot(mbA[sel],-alphaA[sel],'.')
py.ylabel('-alphaA')
py.xlabel('mbA/msun')
py.xlim(0.,3.)
py.ylim(0.,0.7)
py.savefig(outputplotdir+'alphaA_vs_mass.png')
raw_input('enter')

py.clf()
py.plot(mA[sel],IA[sel]/(msun*(grav*msun/light**2.)**2.),'.')
py.plot(mbA[sel],IA[sel]/(msun*(grav*msun/light**2.)**2.),'.')
py.ylabel('IA/msun(G msun/c2)2')
py.xlabel('mbA/msun')
py.xlim(0.,3.5)
py.ylim(0.,100)
py.savefig(outputplotdir+'momin_vs_mass.png')
raw_input('enter')

py.clf()
py.plot(Rns[sel],mA[sel],'.')
py.plot(Rns[sel],mbA[sel],'.',color='red')
py.ylim(0.,5.)
py.xlim(0.,16000)
py.xlabel('Radius/m')
py.ylabel('Mass/msun')
py.savefig(outputplotdir+'mass_vs_rad.png')
raw_input('enter')

print 'I could get rid of unstable solutions, where the mass decreases for increasing pressure.'
py.clf()
py.plot(pcen[sel],mA[sel],'.')
py.plot(pcen[sel],mbA[sel],'.',color='red')
py.xscale('log')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('Mass/msun')
py.savefig(outputplotdir+'mass_vs_pcen.png')
raw_input('enter')

py.clf()
py.plot(pcen[sel],abs(alphaA[sel]),'.')
py.xscale('log')
py.yscale('log')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('abs(alphaA)')
py.savefig(outputplotdir+'alphaA_vs_pcen.png')
raw_input('enter')

phi0sel=phi0[sel]
mbAsel=mbA[sel]
mAsel=mA[sel]

masssel=(1.3<mAsel)&(mAsel<1.35)

py.clf()
py.plot(phi0sel[masssel], np.log(mAsel[masssel]),'.')
py.xscale('log')
py.yscale('log')
raw_input('enter')

