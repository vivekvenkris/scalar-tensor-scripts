#!/usr/bin/env python
##############
#This script should:
#(1) Load the data produced by mathematica and apply some physical checks on it.
#(2) Export data that can be used by Vivek's mass-mass script.
##############

import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI

inputdir='../../data/ANALYSIS/'
solnum='013' #Number of solutions to import ('all' to select all together; but only if all files have the same beta0, which is no more the case, so do not use 'all'!).
outputplotdir='../../plots/' #Output directory for plots.

#Define physical conditions.
absalphaA_max=1. #Maximum value of |alphaA|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=15000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30

##############
#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]
if solnum=='all':
	par_to_load=[]
	sol_to_load=[]
	for fili in os.listdir(inputdir):
		if fili[0:11]=='parameters_':
			par_to_load.append(inputdir+fili)
			sol_to_load.append(inputdir+'solutions_'+fili[11:14]+'.txt')
	for fili in xrange(len(par_to_load)):
		if fili==0:
			params=np.loadtxt(par_to_load[fili])
			sols=np.loadtxt(sol_to_load[fili])
		else:
			new_params=np.loadtxt(par_to_load[fili])
			new_sols=np.loadtxt(sol_to_load[fili])
			if not np.allclose(params,new_params):
				print 'Parameters are not equal! Exiting...'
				exit()
			else:
				sols=np.vstack((sols,new_sols))
else:
	params=np.loadtxt(inputdir+'parameters_'+solnum+'.txt')
	sols=np.loadtxt(inputdir+'solutions_'+solnum+'.txt')

sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
sM=params[1] #Typical radius of the NS in m.
psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
mtb=params[4] #Baryonic mass (kg).
nt0=params[5] #Baryon number density (m^-3).
Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
kns=params[7] #Dimensionless constant of the polytrope EoS.
beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.

if beta0>0:
	ofile='../../data/ANALYSIS/beta0_p%i.txt' %int(beta0) #Name of output file.
else:
	ofile='../../data/ANALYSIS/beta0_m%i.txt' %int(beta0) #Name of output file.

#Variable parameters.
#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
Rns=sols[:,2] #Radius of the NS in m.
alphaA=sols[:,3] #Effective coupling (dimensionless).
phi0=sols[:,4] #Scalar field at infinity (dimensionless).
mA=sols[:,5] #Mass in solar masses.
mbA=sols[:,6] #Mass (bar) in solar masses.
IA=sols[:,7] #Moment of inertia in kg m^2.
alpha0=sols[:,8]
############################
#Take only physical solutions.
check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alphaA)<absalphaA_max)
pcen=pcen[check]
phicen=phicen[check]
Rns=Rns[check]
alphaA=alphaA[check]
phi0=phi0[check]
mA=mA[check]
mbA=mbA[check]
IA=IA[check]
alpha0=alpha0[check]
############################
#Select only those solutions that are consistent with Solar System experiments.
#phi0lim=0.0043 #Limit on phi0 from Solar System experiments (from DEF93).
#phi0lim=0.0024 #Limit on phi0 from Solar System experiments (from DEF96).
#sel=(abs(phi0-phi0lim)<phi0lim*0.05) #This selects only one stripe at the maximum value.
#sel=check&(phi0<phi0lim) #This selects all values consistent with the limit.
############################
#Select only stable solutions, where mass decreases for increasing pressure.
#sel=check&(pcen<6.8e34) #This pressure is chosen (by eye) at the point where mass starts to decrease for increasing pressure.
############################

#Now I should create two interpolated surfaces:
#(A) One for phi0, mbA, mA.
#(B) Another for phi0, mbA, alphaA.

#mA_f=ip.interp2d(phi0[step], mbA[step], mA[step], kind='linear') #I will calculate alphaA from each stripe of constant mbA.
#alphaA_f=ip.interp2d(phi0[step], mbA[step], alphaA[step], kind='linear') #I will compare the calculated alphaA with stripes of this one. Then I will calculate betaA from stripes of constant mbA.
#By doing this I obtain an overflow error: too many data points to interpolate (this may or may not happen in the cluster). I will split the regions in intervals of baryonic mass.

print 'Interesting: the region of consistent alphaA changes depending on whether I add the condition of stable solutions or not. Is it because of the interpolation method?'

lphi0=np.log10(phi0)
points=np.vstack((lphi0,mbA)).T
mA_f=ip.LinearNDInterpolator(points,mA)
alphaA_f=ip.LinearNDInterpolator(points, alphaA)
IA_f=ip.LinearNDInterpolator(points, IA)
alpha0_f=ip.LinearNDInterpolator(points, alpha0)

lphi0min=min(lphi0)
lphi0max=max(lphi0)
lphi0bins=500
lphi0vec=np.linspace(lphi0min, lphi0max, lphi0bins) #Otherwise maybe logspace.
mbAmin= min(mbA)
mbAmax= max(mbA)
mbAbins=500
mbAvec=np.linspace(mbAmin, mbAmax, mbAbins)
alphaAcalc_mat=np.zeros((lphi0bins-1,mbAbins))
alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
kA_mat=np.zeros(np.shape(alphaAcalc_mat))
alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
mA_mat=np.zeros(np.shape(alphaAcalc_mat))
for mbi in xrange(len(mbAvec)):
	mAvec=mA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi]) #Vector of mA as a function of phi0 along the stripe of constant mbA.
	mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])

	alphaAcalc_mat[:,mbi]=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
	alphaAtrue=alphaA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
	alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])

	IAvec=IA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
	kA_mat[:,mbi]=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

	alpha0true=alpha0_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
	alpha0_mat[:,mbi]=0.5*(alpha0true[1:]+alpha0true[:-1])

tol=5 #Tolerance: percentage to which the derived and "true" alphaA must coincide.

alphaAtrue_m=0.5*(alphaAtrue_mat[1:,:]+alphaAtrue_mat[:-1,:])
alphaAcalc_m=0.5*(alphaAcalc_mat[1:,:]+alphaAcalc_mat[:-1,:])
lphi0vec_m=0.5*(lphi0vec[1:]+lphi0vec[:-1])
betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(lphi0vec_m)).T
kA_m=0.5*(kA_mat[1:,:]+kA_mat[:-1,:])
alpha0_m=0.5*(alpha0_mat[1:,:]+alpha0_mat[:-1,:])
mA_m=0.5*(mA_mat[1:,:]+mA_mat[:-1,:])

#perc_mat=abs( (alphaAtrue_mat-alphaAcalc_mat)*100./alphaAtrue_mat )
#good_mat=np.zeros(np.shape(perc_mat))
#perc_mat[np.isnan(perc_mat)]=100.
#good_mat[perc_mat<tol]=1

#Select points where alphaA calculated ant theoretical agree within 5%.
perc_mat=abs( (alphaAtrue_m-alphaAcalc_m)*100./alphaAtrue_m )
good_mat=np.zeros(np.shape(perc_mat))
perc_mat[np.isnan(perc_mat)]=100.
good_mat[perc_mat<tol]=1

py.ion()
alphaAtrue_plot=alphaAtrue_m.copy()
alphaAtrue_plot[good_mat==0]=np.nan
py.contourf(lphi0vec_m[:-1], mbAvec, -alphaAtrue_plot.T)
#alphaAcalc_plot=alphaAcalc_mat.copy()
#alphaAcalc_plot[good_mat==0]=np.nan
#py.contourf(lphi0vec[:-1], mbAvec, alphaAcalc_plot.T)
py.xlabel('log10(Scalar field at infinity)')
py.ylabel('Baryonic mass / solar mass')
py.title('-alphaA')
py.colorbar()
raw_input('enter')
py.clf()
betaA_plot=betaA_mat.copy()
betaA_plot[good_mat==0]=np.nan
py.contourf(lphi0vec_m[:-1], mbAvec, -betaA_plot.T)
py.xlabel('log10(Scalar field at infinity)')
py.ylabel('Baryonic mass / solar mass')
py.title('-betaA')
py.colorbar()
raw_input('enter')
py.clf()
kA_plot=kA_m.copy()
kA_plot[good_mat==0]=np.nan
py.contourf(lphi0vec_m[:-1], mbAvec, kA_plot.T)
py.xlabel('log10(Scalar field at infinity)')
py.ylabel('Baryonic mass / solar mass')
py.title('kA')
py.colorbar()
raw_input('enter')
py.clf()
alpha0_plot=alpha0_m.copy()
alpha0_plot[good_mat==0]=np.nan
py.contourf(lphi0vec_m[:-1], mbAvec, abs(alpha0_plot.T))
py.xlabel('log10(Scalar field at infinity)')
py.ylabel('Baryonic mass / solar mass')
py.title('|alpha0|')
py.colorbar()
raw_input('enter')

#Output a set of points that do not have nans.
selecti=(-np.isnan(alphaAtrue_m))&(-np.isnan(betaA_mat))&(-np.isnan(kA_m))&(good_mat==1)
o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], betaA_mat[selecti], kA_m[selecti])).T

#Save data.
np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header='mA/solar_mass, alpha0, alphaA, betaA, kA, (beta0= %.3f)' %beta0)
