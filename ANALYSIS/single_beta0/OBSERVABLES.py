#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#Input parameters.
beta0=2.
ifile='../../data/ANALYSIS/beta0_p2.npy'
mpmin=0.1
mpmax=3.
mpbins=100
mcmin=0.1
mcmax=3.
mcbins=100
Pbdot_err_factor=10.
gamma_err_factor=10.
omdot_err_factor=1000.

#Binary observable parameters, from '../../data/onezero8.par'.
Pb=0.19765096246205615405*CM.day
Pb_err=0.00000000002871417705*CM.day
Pbdot=-3.9011926801913395108e-13
Pbdot_err=4.2168816927995573978e-15
omdot=5.3102830722373918762*np.pi/180.*1./CM.yr
omdot_err=0.00008490666463720403*np.pi/180.*1./CM.yr
gamma=0.00073189513193250703368
gamma_err=0.00000273145999992813
ecc=0.17188259594416682194
ecc_err=0.00000104439455779383

#Load scalar tensor data.
if not os.path.isfile(ifile): #If python file does not exist, load txt and save python file.
	ifile='../../data/ANALYSIS/beta0_p2.txt' #Name of output file.
	# mA/solar_mass, alpha0, alphaA, betaA, kA
	data=np.loadtxt(ifile[:-4]+'.txt', skiprows=1, delimiter=',')
	mA=data[:,0]
	alpha0=data[:,1]
	alphaA=data[:,2]
	betaA=data[:,3]
	kA=data[:,4]
	#ofile='../../data/ANALYSIS/beta0_p2'
	dicti={'mA':mA, 'alpha0':alpha0, 'alphaA':alphaA, 'betaA':betaA, 'kA':kA}
	np.save(ifile[:-4], dicti)
else: #Directly load python file.
	data=np.load(ifile)[()]
	mA=data['mA']
	alpha0=data['alpha0']
	alphaA=data['alphaA']
	betaA=data['betaA']
	kA=data['kA']
alphaB=alpha0
betaB=beta0*np.ones(len(alpha0))

#Mass-mass diagram.
mpvec=np.linspace(mpmin, mpmax, mpbins)
mcvec=np.linspace(mcmin, mcmax, mcbins)
MP,MC=np.meshgrid(mpvec, mcvec)

py.ion()

#list_ind=np.arange(len(alphaA))
#np.random.shuffle(list_ind)

alpha0_consistent=[]

t=time_estimate(len(alphaA)) #A class that prints estimated computation time.

for i in xrange(len(alphaA)):
#for i in list_ind:
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	alphaA_i=alphaA[i]
	alphaB_i=alphaB[i]
	betaA_i=betaA[i]
	betaB_i=betaB[i]
	kA_i=kA[i]
	
	#print alphaA_i, alphaB_i, betaA_i, betaB_i, kA_i
	monopole, dipole, quadrupole_phi, quadrupole_g=CM.Pbdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)
	Pbdot_mat=monopole+dipole+quadrupole_phi+quadrupole_g
	gamma_mat=CM.gamma_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, kA_i)
	omdot_mat=CM.omdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)

	#Selections.
	Pbdot_sel=(abs(Pbdot_mat-Pbdot)<Pbdot_err*Pbdot_err_factor)
	gamma_sel=(abs(gamma_mat-gamma)<gamma_err*gamma_err_factor)
	omdot_sel=(abs(omdot_mat-omdot)<omdot_err*omdot_err_factor)
	all_sel=(Pbdot_sel&gamma_sel&omdot_sel)
	overlap=len(all_sel[all_sel])
	if overlap>0:
		#print 'CONSISTENT WITH OBSERVATIONS!'
		alpha0_consistent.append(alphaB_i)
	else:
		#print 'Not consistent!'
		pass
	#print

	#Plot.
	numlevels=30
	levels=np.logspace(np.log10(np.amin(-Pbdot_mat)), np.log10(np.amax(-Pbdot_mat)), numlevels)

	Pbdot_yes=np.zeros(np.shape(Pbdot_mat))
	Pbdot_yes[Pbdot_sel]=1
	gamma_yes=np.zeros(np.shape(gamma_mat))
	gamma_yes[gamma_sel]=1
	omdot_yes=np.zeros(np.shape(omdot_mat))
	omdot_yes[omdot_sel]=1

	#py.clf()
	#py.contourf(MP, MC, Pbdot_yes, cmap='ocean_r', alpha=0.5)
	#py.contourf(MP, MC, gamma_yes, cmap='ocean_r', alpha=0.5)
	#py.contourf(MP, MC, omdot_yes, cmap='ocean_r', alpha=0.5)
	#raw_input('enter')

print np.amax(alpha0_consistent)





