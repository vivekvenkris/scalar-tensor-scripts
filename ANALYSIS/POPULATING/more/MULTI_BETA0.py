#!/usr/bin/env python
import numpy as np
import pylab as py
import os

beta0_vec=np.linspace(-6., 6, 30)

for i in xrange(len(beta0_vec)):
	beta0_i=beta0_vec[i]
	print 'Analysis for beta0=%f running... (%i / %i)' %(beta0_i,i+1,len(beta0_vec))
	os.system('./ANALYSIS_single_beta0.m %f' %beta0_i)