#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30
day=24.*3600.
yr=365.*day

#Define some functions useful for this analysis.
def py2math(variable):
	'''Replaces an input variable in format %e by the corresponding Mathematica symbol.'''
	return str('%e' %variable).replace('e', ' 10^')

def input_par_math_file(sp=None, sM=None, pNsurT=None, rhoNcenT=None, inputdir=None, outputdir=None, mtb=None, nt0=None, Gamma=None, kns=None, afun=None, alpha=None, beta0=None):
	'''It inputs the parameters to be used in the Mathematica calculation, and will update the INPUT_PARAMETERS.m file.'''
	files_dir='./Eq_solver/'
	default_file=files_dir+'DEFAULT_PARAMETERS.m'
	input_file=files_dir+'INPUT_PARAMETERS.m'

	#Load default parameters, replace undefined parameters by default.
	import Eq_solver.DEFAULT_PARAMETERS as DEF
	Gn, c, msun, rsun=py2math(DEF.Gn), py2math(DEF.c), py2math(DEF.msun), py2math(DEF.rsun)
	#if not sp: sp=py2math(DEF.sp)
	sp = py2math(DEF.SM) if not sp else py2math(sp)
	sM = py2math(DEF.sM) if not sM else py2math(sM)
	pNsurT = py2math(DEF.pNsurT) if not pNsurT else py2math(pNsurT)
	rhoNcenT = py2math(DEF.rhoNcenT) if not rhoNcenT else py2math(rhoNcenT)
	inputdir = DEF.inputdir if not inputdir else inputdir
	outputdir = DEF.outputdir if not outputdir else outputdir
	mtb = py2math(DEF.mtb) if not mtb else py2math(mtb)
	nt0 = py2math(DEF.nt0) if not nt0 else py2math(nt0)
	Gamma = py2math(DEF.Gamma) if not Gamma else py2math(Gamma)
	kns = py2math(DEF.kns) if not kns else py2math(kns)
	afun = DEF.afun if not afun else afun
	alpha = DEF.alpha if not alpha else alpha
	beta0 = py2math(DEF.beta0) if not beta0 else py2math(beta0)
	mathvars=(Gn, c, msun, rsun, sp, sM, pNsurT, rhoNcenT, inputdir, outputdir, mtb, nt0, Gamma, kns, afun, alpha, beta0)
	file=open(input_file,'w')
	file.write('Gn=%s; \nc=%s; \nMsun=%s; \nrsun=%s; \nsp=%s; \nsM=%s; \npNsurT=%s; \n\[Rho]NcenT=%s; \ninputdir="%s"; \noutputdir="%s"; \nmtb=%s; \nnt0=%s; \n\[CapitalGamma]=%s; \nkns=%s; \n%s; \n%s; \n\[Beta]0=%s; \n' %mathvars)
	file.close()

def eq_solver():
	'''Solves the system of differential equations using the Mathematica script.'''
	os.system('cd Eq_solver; ./Eq_solver.m')
	return

#Define some functions.

def Pbdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	'''mma and mmb are in solar masses.'''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	monopole1=-3.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm**(5./3.)*ecc*ecc*(1.+ecc*ecc*1./4.)*1./(1.-ecc*ecc)**(7./2.)
	monopole2=(5./3.*(alphaA+alphaB)-2./3.*(alphaA*XA+alphaB*XB)+(betaA*alphaB+betaB*alphaA)*1./(1.+alphaA*alphaB))**2.
	monopole=monopole1*monopole2

	dipole1=-2.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm*(1.+ecc*ecc*1./2)/(1.-ecc*ecc)**(5./2.)*(alphaA-alphaB)**2.
	dipole2=-4.*np.pi/(1.+alphaA*alphaB)*nu*GABterm**(5./3.)*1./(1.-ecc*ecc)
	dipole3=8./5.*(1.+31.*ecc*ecc/8.+19.*ecc**4./32.)*(alphaA-alphaB)*(alphaA*XA+alphaB*XB)*(XA-XB)+(1.+3.*ecc*ecc+3.*ecc**4./8.)*(alphaA-alphaB)*(betaB*alphaA*XA-betaA*alphaB*XB)*1./(1.+alphaA*alphaB)
	dipole=dipole1-dipole2*dipole3

	quadrupole_g=-192.*np.pi*nu/(5.*(1.+alphaA*alphaB))*GABterm**(5./3.)*(1.+73.*ecc*ecc/24.+37.*ecc**4./96.)/(1.-ecc*ecc)**(7./2.)

	quadrupole_phi=quadrupole_g*1./6.*((alphaA+alphaB)-alphaA*XA+alphaB*XB)**2.
	return monopole, dipole, quadrupole_phi, quadrupole_g

def gamma_f(mma, mmb, Pb, ecc, alphaA, alphaB, kA):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return ecc*XB*1./(n*(1.+alphaA*alphaB))*GABterm**(2./3.)*(XB*(1.+alphaA*alphaB)+1.+kA*alphaB)

def omdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return 3.*n*1./(1.-ecc*ecc)*GABterm**(2./3.)*((1.-1./3.*alphaA*alphaB)*1./(1.+alphaA*alphaB)-(XA*betaB*alphaA*alphaA+XB*betaA*alphaB*alphaB)*1./(6.*(1.+alphaA*alphaB)**2.))



