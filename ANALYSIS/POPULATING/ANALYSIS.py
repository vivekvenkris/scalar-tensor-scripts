#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
import COMMON as CM

#INPUT PARAMETERS:
beta0=-5.
beta0vec=np.array([-6.,-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.,6.])

sp = 1e36 #Typical maximum pressure at the centre of the NS in Pa.
sM = 1e4 #Typical radius of the NS in m.
pNsurT = 1e-6 #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
pNminT = 0.002 #Minimum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
pNmaxT = 0.2 #Maximum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
phiNminT = 0.001 #Minimum value of the scalar field at the centre of the NS.
phiNmaxT = 0.5 #Maximum value of the scalar field at the centre of the NS.
rhoNcenT = 1e-2 #Radial coordinate at the centre (in theory should be zero).
pbins = 100 #Number of bins in values of pressure (log spaced).
phibins = 100 #Number of bins in values of scalar field (log spaced).
plotdir='./plots/'
mbAbins=20 #Number of points in the vector of baryonic mass.
#py.ion()

#Create folder for plots.
filenum=[]
for fili in os.listdir(plotdir): filenum.append(int(fili[1:]) if fili[0]=='p' else 0)
odir=plotdir+'p%i' %(max(filenum)+1)
os.makedirs(odir)

for beta0i in xrange(len(beta0vec)):
	beta0=beta0vec[beta0i]
	print 'Beta0=%.3f' %beta0
	tempdir='./Eq_solver/TEMP/'
	#CM.input_par_math_file()
	CM.input_par_math_file(sp=1e36, sM=sM, pNsurT=pNsurT, rhoNcenT=rhoNcenT, beta0=beta0)

	#Initial conditions.
	pNcenT_vec=np.logspace(np.log10(pNminT), np.log10(pNmaxT), pbins)
	phiNcenT_vec=np.logspace(np.log10(phiNminT), np.log10(phiNmaxT), phibins)
	input=np.vstack((pNcenT_vec,phiNcenT_vec)).T
	#Create input file with initial conditions.
	np.savetxt(tempdir+'input.txt', input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')

	#Solve equations.
	CM.eq_solver()

	#Load solutions.
	#indet_data=np.loadtxt(tempdir+'output.txt', dtype='str', usecols=(8,))
	#c0=indet_data!='Indeterminate' #This condition will eliminate "Indeterminate" solutions.
	#raw_data[raw_data=='Indeterminate']='999999'
	#sols=raw_data.astype(float)



	raw_data=np.loadtxt(tempdir+'output.txt', dtype='str')
	c0=(raw_data[:,8]!='Indeterminate') #This condition will eliminate "Indeterminate" solutions.
	#Convert data to floats (first get rid of "Indeterminate" rows).
	raw_data[raw_data=='Indeterminate']='999999'
	sols=raw_data.astype(float)
	pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (baryonic) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).

	#Physical conditions.
	c1=(phicen*phi0>=0) #As explained in DamourEspositoFarese1998 (referring to DamourEspositoFarese1996), configurations where that product is negative are energetically disfavored.
	c2=(Rns>0.)&(mA>0.)&(mbA>0.) #NS radius must be positive.
	c3=(abs(alpha0)<1) #|alpha0| cannot be larger than 1.

	sel=c0&c1&c2&c3
	#mbA_choice=1.
	#mbAsel=(abs(mbA-mbA_choice)<0.01)

	mbAvec=np.linspace(min(mbA[sel]), max(mbA[sel]), mbAbins)
	print 'Total number of solutions: ', len(c0)
	print 'Solutions with singularities: ', len(c0[-c0])
	print 'Solutions energetically disfavoured: ', len(c1[-c1])
	print 'Solutions with negative NS radius or mass: ', len(c2[-c2])
	print 'Solutions with |alpha0|>1: ', len(c3[-c3])
	print 'Total number of good solutions: ', len(sel[sel])
	#print 'Solutions with the chosen mass: ', len(mbAsel[mbAsel])
	print

	#py.figure(beta0i+1)
	py.clf()
	plottitle=(str('%.3f' %beta0)).replace('.','p')
	#py.loglog(pcen, phicen,'.', color='blue', alpha=0.5, label='All')
	py.loglog(pcen[-c0], phicen[-c0], 's', color='red', alpha=0.5, label='Singularities')
	py.loglog(pcen[-c1], phicen[-c1], '^', color='magenta', alpha=0.5, label='Energetically disfavoured')
	py.loglog(pcen[-c2], phicen[-c2], '^', color='yellow', alpha=0.5, label='Negative mass or radius')
	py.loglog(pcen[-c3], phicen[-c3], '*', color='brown', alpha=0.5, label='|alpha0|>1')
	py.loglog(pcen[sel], phicen[sel], '.', color='green', alpha=1, label='Good solutions')
	for mbAi in xrange(len(mbAvec)):
		mbAsel=(abs(mbA-mbAvec[mbAi])<0.01)
		print len(mbAsel[mbAsel])
		print mbAvec[mbAi]
		print
		py.loglog(pcen[mbAsel], phicen[mbAsel], 'o', alpha=0.8)
	py.title('Beta0=%.3f' %beta0)
	py.legend(loc='lower right')
	py.xlabel('Pressure at the centre of the NS/Pa')
	py.ylabel('Scalar field at the centre of the NS')
	py.savefig('%s/%i_beta0_%s.png' %(odir,beta0i,plottitle))
	#sleep(1)




#raw_input('enter')
exit()
sel=c0&c1&c2&c3 #Selection on all conditions together.
pcen=pcen[sel]
phicen=phicen[sel]
Rns=Rns[sel]
alphaA=alphaA[sel]
phi0=phi0[sel]
mA=mA[sel]
mbA=mbA[sel]
IA=IA[sel]
alpha0=alpha0[sel]

print 'The output of this script has passed physical conditions (on maximum value of alpha0, certain range of radius and NS masses). But it will need further selections:'
print '(1) Take only physical solutions, i.e. those where mass (mA) decreases for increasing pressure (at the centre of the NS).'
print '(2) The theoretical alphaA and the derived one should coincide within 5 percent.'

exit()




lphi0bins=400 #Number of points in the vector of interpolated values of the scalar field at infinity, log10(phi0), in order to calculate alphaA, betaA, and kA.
mbAbins=400 #Number of points in the vector of baryonic mass.
inputdir='../../../data/ANALYSIS/multi_beta0/run4/'
outputdir='../../../data/ANALYSIS/multi_beta0/run4/combined/'

###########################
#Create output folder if not existing.
if not os.path.isdir(outputdir): os.makedirs(outputdir)

#Define physical conditions.
absalpha0_max=1. #Maximum value of |alpha0|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=20000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.

##############
#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]

param_files=np.sort([ fili for fili in os.listdir(inputdir) if fili[0:10]=='parameters'])

for fili in xrange(len(param_files)):
	print 'File %i / %i .' %(fili+1, len(param_files))
	indi=param_files[fili][-7:-4]
	params=np.loadtxt(inputdir+'parameters_%s.txt' %indi)
	sols=np.loadtxt(inputdir+'solutions_%s.txt' %indi)
	ofile=outputdir+'combined_%s.txt' %indi
	
	sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
	sM=params[1] #Typical radius of the NS in m.
	psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
	rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
	mtb=params[4] #Baryonic mass (kg).
	nt0=params[5] #Baryon number density (m^-3).
	Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
	kns=params[7] #Dimensionless constant of the polytrope EoS.
	beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.

	#Variable parameters.
	#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
	pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (bar) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8]

	############################
	#Take only physical solutions.
	check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alpha0)<absalpha0_max)
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]

	#Interpolate all quantities on the log10(phi0)-mb plane.
	lphi0=np.log10(phi0)
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)#########

	lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	alphaAcalc_mat=np.zeros((lphi0bins-1,mbAbins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))########

	for mbi in xrange(len(mbAvec)):
		mAvec=mA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi]) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])

		alphaAcalc_mat[:,mbi]=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		alphaAtrue=alphaA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])

		IAvec=IA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		kA_mat[:,mbi]=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		alpha0true=alpha0_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alpha0_mat[:,mbi]=0.5*(alpha0true[1:]+alpha0true[:-1])
	
		pcenvec=pcen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])########
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])######
	
	#tol=5 #Tolerance: percentage to which the derived and "true" alphaA must coincide.

	alphaAtrue_m=0.5*(alphaAtrue_mat[1:,:]+alphaAtrue_mat[:-1,:])
	alphaAcalc_m=0.5*(alphaAcalc_mat[1:,:]+alphaAcalc_mat[:-1,:])
	lphi0vec_m=0.5*(lphi0vec[1:]+lphi0vec[:-1])
	betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(lphi0vec_m)).T
	kA_m=0.5*(kA_mat[1:,:]+kA_mat[:-1,:])
	alpha0_m=0.5*(alpha0_mat[1:,:]+alpha0_mat[:-1,:])
	mA_m=0.5*(mA_mat[1:,:]+mA_mat[:-1,:])
	pcen_m=0.5*(pcen_mat[1:,:]+pcen_mat[:-1,:])############

	#Select points where alphaA calculated ant theoretical agree within 5%.
	#perc_mat=abs( (alphaAtrue_m-alphaAcalc_m)*100./alphaAtrue_m )
	#good_mat=np.zeros(np.shape(perc_mat))
	#perc_mat[np.isnan(perc_mat)]=100.
	#good_mat[perc_mat<tol]=1

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_m))&(-np.isnan(betaA_mat))&(-np.isnan(kA_m))
	#o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti])).T
	o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti], pcen_m[selecti])).T

	#Save data.
	np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n psur=%e \n rhocen=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa.' %(beta0, sp, sM, psur, rhocen, mtb, nt0, Gamma, kns))


