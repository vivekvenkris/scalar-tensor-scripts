Gn=6.673000 10^-11; 
c=2.997925 10^+08; 
Msun=1.989100 10^+30; 
rsun=6.955000 10^+08; 
sp=1.000000 10^+36; 
sM=1.000000 10^+04; 
pNsurT=1.000000 10^-06; 
\[Rho]NcenT=1.000000 10^-02; 
inputdir="./TEMP/"; 
outputdir="./TEMP/"; 
mtb=1.660000 10^-27; 
nt0=1.000000 10^+44; 
\[CapitalGamma]=2.340000 10^+00; 
kns=1.950000 10^-02; 
A[\[Beta]0_, \[CurlyPhi]_] = Exp[1/2 \[Beta]0 \[CurlyPhi]^2]; 
\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]; 
\[Beta]0=6.000000 10^+00; 
