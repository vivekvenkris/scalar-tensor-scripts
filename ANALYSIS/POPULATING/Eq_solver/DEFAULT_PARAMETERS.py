Gn = 6.673e-11 #Newtons gravitational constant.
c = 2.99792458e8 #Speed of light in vacuum.
msun = 1.9891e30 #Solar mass.
rsun = 6.955e8 #Solar radius.

#Specific input parameters.
sp = 1e36 #Typical maximum pressure at the centre of the NS in Pa.
sM = 1e4 #Typical radius of the NS in m.
pNsurT = 1e-6 #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
pNminT = 0.002 #Minimum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).

pNmaxT = 0.2 #Maximum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
phiNminT = 0.001 #Minimum value of the scalar field at the centre of the NS.
phiNmaxT = 0.5 #Maximum value of the scalar field at the centre of the NS.
rhoNcenT = 1e-2 #Radial coordinate at the centre (in theory should be zero).
pbins = 400 #Number of bins in values of pressure (log spaced).
phibins = 400 #Number of bins in values of scalar field (log spaced).
outputdir = "./TEMP/" #Output directory of data.
inputdir = "./TEMP/" #Input directory of data.

#Model for the Equation of state of the neutron star.

#Polytrope (from DamourEspositoFarese1996).

mtb = 1.66e-27 #Baryonic mass (kg).
nt0 = 1e44 #Baryon number density (m^-3).
Gamma = 2.34 #Dimensionless constant.
kns = 0.0195 #Dimensionless constant.

#Model for the scalar tensor theory.

afun='A[\[Beta]0_, \[CurlyPhi]_] = Exp[1/2 \[Beta]0 \[CurlyPhi]^2]'
alpha='\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]'
beta0=-3
